/** 
 * Spotify v 0.0.1
 * February 2021
 * Cesar Venzor <cesarvenzor@protonmail.com>
 **/

/* PROPERTIES */
spotifyCode = "answer.svg";
logo = "spotifyLogo.svg";

wallThickness = 4;
plateThickness = 2;
tolerance = 0.2;

x = 70;
y = 20;
z = 2;

base_radius = 5;
code_radius = 2;
$fn = 50;

object = "code"; // ["code", "base"]
 
 
/* MAIN CODE */

if (object == "code") {
  code_body(x, y, code_radius, z);

} else if (object == "base") {
  rounded_base(x, y, base_radius, code_radius, z);
}


/* MODULES */

module spotify_logo(height) {
  linear_extrude(height = height) translate([0,0,0]) scale([0.25, 0.25, 1])
  import (logo);
}

module bar_code(height) {
    linear_extrude(height = height) translate([0,0,0]) scale([0.35, 0.35, 1])
    import (spotifyCode);
}
 
module rounded_box_body(x, y, radius, height, center) {
  actual_z = center == true ? -height / 2 : 0;
  
  hull() {
    translate([-x/2 + radius, -y/2 + radius, actual_z]) cylinder(r = radius, h = height);
    translate([-x/2 + radius, y/2 - radius, actual_z]) cylinder(r = radius, h = height);
    translate([x/2 - radius, -y/2 + radius, actual_z]) cylinder(r = radius, h = height);
    translate([x/2 - radius, y/2 - radius, actual_z]) cylinder(r = radius, h = height);
  }
}

module rounded_base(x, y, ext_radius, int_radius, height) {
  difference() {
    rounded_box_body(x + wallThickness, y + wallThickness, ext_radius, height + plateThickness, center = false);
  
    translate([0,0, plateThickness - tolerance])
    rounded_box_body(x + tolerance, y + tolerance, int_radius, height * 2, center = false);
  }
}

module code_body(x, y, radius, height) {
  difference() {
    rounded_box_body(x,y,radius,height);
    
    translate([-x / 2 - 7, -y / 2 - 0.5, -.1]) 
    bar_code(height + 0.2);
  }

  translate([-x / 2 - 1, -y / 2 + 2, height]) 
    spotify_logo(0.8);
}