---

Spotify barcode

---

## Instructions

1. Download your code as svg from https://www.spotifycodes.com/
2. Use Inkscape to remove the svg background and the Spotify logo.
3. Save this modified svg in the same directory as the OpenSCAD file.
4. Change line 8 to include your svg file.
   spotifyCode = "answer.svg";
5. In line 23 choose which object you want to generate (code for the barcode cut, or base).
